function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Component = /*#__PURE__*/function () {
  function Component(tagName, attribute, children) {
    _classCallCheck(this, Component);

    _defineProperty(this, "tagName", void 0);

    _defineProperty(this, "attribute", void 0);

    _defineProperty(this, "children", void 0);

    this.tagName = tagName;
    this.attribute = attribute;
    this.children = children;
  }

  _createClass(Component, [{
    key: "renderAttribute",
    value: function renderAttribute() {
      return "<".concat(this.tagName, " ").concat(this.attribute.name, "=\"").concat(this.attribute.value, "\"/>");
    }
  }, {
    key: "render",
    value: function render() {
      if (!(this.children == null || this.children == undefined)) {
        return "<".concat(this.tagName, ">").concat(this.children, "</").concat(this.tagName, ">");
      } else {
        if (!(this.attribute == null || this.attribute == undefined)) {
          return this.renderAttribute();
        } else {
          return "<".concat(this.tagName, "/>");
        }
      }
    }
  }]);

  return Component;
}();

export { Component as default };
//# sourceMappingURL=Component.js.map