import data from './data.js';
import Component from './components/Component.js';
const title = new Component('h1', null, ['La', ' grosse ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();
import Img from './components/Img.js';
const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
document.querySelector('.pageContent').innerHTML = c.render();
