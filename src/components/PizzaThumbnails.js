import Component from "./Component"

export default class pizzaThumbnail extends Component {
    data
    constructor(data) {
        this.data=data;
    }
	render() {
        const li=[
            new Component('li',`Prix petit format : ${data.price_small}`),
            new Component('li',`Prix grand format : ${data.price_large}`,
        ];
        const lu=new Component('ul',li);
        const h4=new Component('h4',data.name);
        const section=new Component('section',[h4,ul]);
        const img=new Img(data.image);
        const a = new Component('a',{name:href,value:data.image},[img,section]);
        const article=new Component('article',
        {name:class,value:"pizzaThumbnail"},a);
        this.data.render();
    }
}
