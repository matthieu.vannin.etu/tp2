export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	renderAttribute() {
		return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
	}
	renderChildren() {
		if (this.children instanceof Array) {
			let str = '';
			for (let i = 0; i < this.children.length; i++) {
				if (this.children[i] instanceof Component) {
					str = str + this.children[i].render();
				} else {
					str = str + this.children[i];
				}
			}
			return `<${this.tagName}>${str}</${this.tagName}>`;
		}
		return `<${this.tagName}>${this.children}</${this.tagName}>`;
	}
	render() {
		if (!(this.children == null || this.children == undefined)) {
			return this.renderChildren();
		} else {
			if (!(this.attribute == null || this.attribute == undefined)) {
				return this.renderAttribute();
			} else {
				return `<${this.tagName}/>`;
			}
		}
	}
}
