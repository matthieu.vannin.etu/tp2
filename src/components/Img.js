import Component from './Component.js';
export default class Img extends Component {
	constructor(valeur) {
		super('img', { name: 'src', value: valeur });
	}
}
